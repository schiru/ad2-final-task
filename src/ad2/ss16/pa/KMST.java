package ad2.ss16.pa;

import java.util.*;

/**
 * Klasse zum Berechnen eines k-MST mittels Branch-and-Bound. Hier sollen Sie
 * Ihre L&ouml;sung implementieren.
 */
public class KMST extends AbstractKMST {

	private int numNodes;
	private int numEdges;
	private HashSet<Edge> edges;
	private SortedSet<Edge> sortedEdges;
	private SortedSet<Edge> sortedEdgesByNodeWeight;
	private boolean[] nodesVisited;
	private int k;
	private Set<Edge> checkedEdges = new TreeSet<>();

	private Map<Integer, SortedSet<Edge>> nodeConnections;

	private int bestSolution = Integer.MAX_VALUE;

	/**
	 * Der Konstruktor. Hier ist die richtige Stelle f&uuml;r die
	 * Initialisierung Ihrer Datenstrukturen.
	 * 
	 * @param numNodes
	 *            Die Anzahl der Knoten
	 * @param numEdges
	 *            Die Anzahl der Kanten
	 * @param edges
	 *            Die Menge der Kanten
	 * @param k
	 *            Die Anzahl der Knoten, die Ihr MST haben soll
	 */
	public KMST(Integer numNodes, Integer numEdges, HashSet<Edge> edges, int k) {
		this.numNodes = numNodes;
		this.numEdges = numEdges;
		this.edges = edges;
		this.k = k;
		sortedEdges = new TreeSet<>();
		sortedEdges.addAll(edges);
		nodesVisited = new boolean[numNodes];

		// Generate AdjacencyList
		nodeConnections = new HashMap<>(numNodes, 1.0f);

		for (int i = 0; i < numNodes; ++i) {
			nodeConnections.put(i, new TreeSet<>());
		}

		for(Edge e : edges) {
			nodeConnections.get(e.node1).add(e);
			nodeConnections.get(e.node2).add(e);
		}

		// FIND INIITAL UPPER BOUND
		Edge cheapestEdge = sortedEdges.first();
		Set<Edge> edgesSelected = new HashSet<>();
		edgesSelected.add(cheapestEdge);
		SortedSet<Edge> edgesLeft = new TreeSet<>(sortedEdges);
		edgesLeft.remove(cheapestEdge);

		Set<Edge> edgesToTry = new HashSet<>();
		edgesToTry.addAll(nodeConnections.get(cheapestEdge.node1));
		edgesToTry.addAll(nodeConnections.get(cheapestEdge.node2));

		nodesVisited[cheapestEdge.node1] = true;
		nodesVisited[cheapestEdge.node2] = true;

		BnBSolution localUpperBound = findLocalSolution(cheapestEdge.weight, edgesToTry, edgesSelected, edgesLeft);

		// Check if the found local upper bound is < bestsolution
		if (localUpperBound != null && localUpperBound.getUpperBound() < bestSolution) {
			bestSolution = localUpperBound.getUpperBound();
			setSolution(bestSolution, new HashSet<Edge>(localUpperBound.getBestSolution()));
		}

		nodesVisited = new boolean[numNodes];
	}

	/**
	 * Diese Methode bekommt vom Framework maximal 30 Sekunden Zeit zur
	 * Verf&uuml;gung gestellt um einen g&uuml;ltigen k-MST zu finden.
	 *
	 * <p>
	 * F&uuml;gen Sie hier Ihre Implementierung des Branch-and-Bound Algorithmus
	 * ein.
	 * </p>
	 */
	@Override
	public void run() {
		SortedSet<Edge> sortedEdgesCopy = new TreeSet<>();
		sortedEdgesCopy.addAll(sortedEdges);

		for (Edge e : sortedEdges) {
			findSolution(e, 0, null, new HashSet<Edge>(), sortedEdgesCopy);

			// If we have already tried all possible paths from edge e,
			// we can rest assured that any other edge + any path from e
			// cannot be a better solution than the one we found already.
			checkedEdges.add(e);
		}
	}

	private void findSolution(Edge currentEdge, int previousWeight, PriorityQueue<Edge> adjacentGraphEdgesToTry, Set<Edge> edgesSelected, SortedSet<Edge> edgesLeft) {
		boolean node1Visited = nodesVisited[currentEdge.node1];
		boolean node2Visited = nodesVisited[currentEdge.node2];

		// Prevent adding edges that would create a cycle
		if (node1Visited == true && node2Visited == true)
			return;

		// Select the current edge
		edgesSelected.add(currentEdge);
		edgesLeft.remove(currentEdge);

		// As we're adding an edge, mark both nodes as visited
		nodesVisited[currentEdge.node1] = true;
		nodesVisited[currentEdge.node2] = true;

		// Calculate lower bound heuristic
		// previous weight + current weight + estimate of min future weights to come
		int numEdgesToCome = k-1 - edgesSelected.size();
		int currentRouteWeight = previousWeight + currentEdge.weight;
		int lowerBound = currentRouteWeight + calculateLowerBoundHeuristic(edgesLeft, numEdgesToCome);

		if (lowerBound < bestSolution) {
			SortedSet<Edge> adjacentEdgesStore = new TreeSet<Edge>();
			adjacentEdgesStore.addAll(nodeConnections.get(currentEdge.node1));
			adjacentEdgesStore.addAll(nodeConnections.get(currentEdge.node2));

			if (adjacentGraphEdgesToTry == null)
				adjacentGraphEdgesToTry = new PriorityQueue<Edge>();
			else {
				// create a copy
				adjacentGraphEdgesToTry = new PriorityQueue<Edge>(adjacentGraphEdgesToTry);
			}

			// temporarily remove all adjacent edges for recursion
			// so that the node can't be accessed again
			removeConnections(nodeConnections, adjacentEdgesStore);

			adjacentGraphEdgesToTry.addAll(adjacentEdgesStore);

			if (edgesSelected.size() == k - 1) {
				if (currentRouteWeight < bestSolution) {
					// set route weight (prev weight + current weight) as upper bound (best result found)
					bestSolution = currentRouteWeight;
					setSolution(bestSolution, new HashSet<Edge>(edgesSelected));
				}
			} else {
				// find valid upper heuristic/quick solution
				BnBSolution localUpperBound = findLocalSolution(currentRouteWeight, adjacentGraphEdgesToTry,edgesSelected, edgesLeft);

				// Check if the found local upper bound is < bestsolution
				if (localUpperBound != null && localUpperBound.getUpperBound() < bestSolution) {
					bestSolution = localUpperBound.getUpperBound();
					setSolution(bestSolution, new HashSet<Edge>(localUpperBound.getBestSolution()));
				}

				// If lower bound is lower than bestsolution
				// (and therefor also lower than localUpperBound)
				if (lowerBound < bestSolution) {
					while (!adjacentGraphEdgesToTry.isEmpty()) {
						Edge nextToTry = adjacentGraphEdgesToTry.poll();
						if (!edgesSelected.contains(nextToTry) && !checkedEdges.contains(nextToTry)) {
							findSolution(nextToTry, currentRouteWeight, adjacentGraphEdgesToTry, edgesSelected, edgesLeft);
						}
					}
				}
			}

			// add adjacent nodes back for previous recursion level
			addConnections(nodeConnections, adjacentEdgesStore);
		}

		// Deselect current edge again for previous recursion layer
		edgesLeft.add(currentEdge);
		edgesSelected.remove(currentEdge);

		// Restore old nodesVisited state prior to adding the current edge
		nodesVisited[currentEdge.node1] = node1Visited;
		nodesVisited[currentEdge.node2] = node2Visited;
	}

	// using prim algorithm
	BnBSolution findLocalSolution(int currentRouteWeight, Collection<Edge> adjacentGraphEdgesToTry, Set<Edge> edgesSelected, SortedSet<Edge> edgesLeft) {
		boolean[] nodesVisitedCopy = nodesVisited.clone();
		Set<Edge> edgesSelectedCopy = new HashSet<>(edgesSelected);
		Set<Edge> edgesLeftCopy = new TreeSet<>(edgesLeft);
		List<Edge> edgesToRemove = new ArrayList<>();

		SortedSet<Edge> edgesToTry = new TreeSet<>(adjacentGraphEdgesToTry);

		int size = edgesSelectedCopy.size();
		int edgesToCome = k-1-size;

		while (edgesToCome > 0 && currentRouteWeight < bestSolution && !edgesLeftCopy.isEmpty()) {
			if (!edgesToRemove.isEmpty()) {
				edgesLeftCopy.removeAll(edgesToRemove);
				edgesToTry.removeAll(edgesToRemove);
				edgesToRemove.clear();
			}

			// find cheapest edge that extends current graph
			for (Edge e : edgesToTry) {
				// if node1 and node 2 are already in, we would create a cycle
				if (nodesVisitedCopy[e.node1] && nodesVisitedCopy[e.node2]) {
					// prevent ConcurrentModificationException
					edgesToRemove.add(e);
				}

				// If node1 xor node2 is in the current selection -> node is adjacent
				if (nodesVisitedCopy[e.node1] ^ nodesVisitedCopy[e.node2])
				{
					edgesSelectedCopy.add(e);
					edgesLeftCopy.remove(e);

					edgesToTry.addAll(nodeConnections.get(e.node1));
					edgesToTry.addAll(nodeConnections.get(e.node2));

					currentRouteWeight += e.weight;

					nodesVisitedCopy[e.node1] = true;
					nodesVisitedCopy[e.node2] = true;

					if (--edgesToCome < 1) {
						return new BnBSolution(currentRouteWeight, edgesSelectedCopy);
					}

					// cheapest edge that creates no cycle has been found,
					// start again from cheapest still available edge in next while loop
					// as an edge that hasn't been adjacent before may now be.
					break;
				}
			}

			// If we haven't found ANY edge to add, then we also wouldn't find one in the next iteration of the while
			return null;
		}

		return null;
	}

	int calculateLowerBoundHeuristic(SortedSet<Edge> edgesLeft, int numEdgesToCome) {
		// As the edges left are sorted, this foreach loop iterates ascending by weight
		int countedEdges = 0;
		int heuristic = 0;
		for (Edge e : edgesLeft) {
			heuristic += e.weight;

			if (--numEdgesToCome < 1)
				break;
		}

		return heuristic;
	}

	private void removeConnections(Map<Integer, SortedSet<Edge>> adjacencyList, Collection<Edge> connectionsToRemove) {
		for(Edge e : connectionsToRemove) {
			adjacencyList.get(e.node1).remove(e);
			adjacencyList.get(e.node2).remove(e);
		}
	}

	private void addConnections(Map<Integer, SortedSet<Edge>> adjacencyList, Collection<Edge> connectionsToAdd) {
		for(Edge e : connectionsToAdd) {
			adjacencyList.get(e.node1).add(e);
			adjacencyList.get(e.node2).add(e);
		}
	}
}