# About this Repository

An algorithm written in Java that solves a [k-MST](https://en.wikipedia.org/wiki/K-minimum_spanning_tree) problem using a Branch-And-Bound strategy.

This mini-project was a final task for the university lecture Algorithms and Data Structures 2 at Vienna University of Technology.

## Author
The algorithm within KMST.java was created me, Thomas Jirout ([@_schiru](https://twitter.com/_schiru)).

Other file contents belong to the author named in the very file.